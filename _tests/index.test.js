import { describe, expect, test } from 'vitest'
import { setup, $fetch } from '@nuxt/test-utils'

describe('Página principal', async () => {
    await setup({})

    test('contains string DevOps', async () => {
        const body = await $fetch('/');

        expect(body).toContain('<div class="title-main text-center">DevOps</div>');
    })
})